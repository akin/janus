﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
	public Controllable controllable;
	
    void Update()
    {
		if(controllable != null)
		{
			if(Input.GetKey(KeyCode.Space))
			{
				controllable.jumping = true;
			}
			
			controllable.runningSpeed = Input.GetAxis("Horizontal");
		}
    }
}
 
