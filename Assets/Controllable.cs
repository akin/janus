﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controllable : MonoBehaviour
{
	public float weight = 50.0f;
	public float jumpPower = 5000.0f;
	public float runPower = 1000.0f;
	public Vector3 forward = Vector3.right;
	public Vector3 up = Vector3.up;
	
	private float distanceToGround = 0.0f;
	
	// Doing
	public bool jumping = false;
	public float runningSpeed = 0.0f;

    void Start()
    {
		distanceToGround = GetComponent<Collider>().bounds.extents.y + 0.1f;
		GetComponent<Rigidbody>().mass = weight;
    }
	
	bool IsGrounded()
	{
		return Physics.Raycast(transform.position, -up, distanceToGround);
	}
	
	// https://answers.unity.com/questions/8676/make-rigidbody-walk-like-character-controller.html?page=2&pageSize=5&sort=votes
	private void TrackGrounded(Collision collision)
	{
		/*
		var maxHeight = capsule.bounds.min.y + capsule.radius * .9f;
		foreach (var contact in collision.contacts)
		{
			if (contact.point.y < maxHeight)
			{
				if (isKinematic(collision))
				{
					// Get the ground velocity and we parent to it
					groundVelocity = collision.rigidbody.velocity;
					transform.parent = collision.transform;
				}
				else if (isStatic(collision))
				{
					// Just parent to it since it's static
					transform.parent = collision.transform;
				}
				else
				{
					// We are standing over a dinamic object,
					// set the groundVelocity to Zero to avoid jiggers and extreme accelerations
					groundVelocity = Vector3.zero;
				}
				// Esta en el suelo
				grounded = true;
			}
			break;
		}
		*/
	}
	
	void FixedUpdate()
	{
		if(IsGrounded())
		{
			if(jumping)
			{
				GetComponent<Rigidbody>().AddForce(up * jumpPower);
			}
			GetComponent<Rigidbody>().AddForce(forward * runningSpeed * runPower);
		}
		else
		{
		}
		
		// Single shots need to be reset at end
		jumping = false;
	}
}
